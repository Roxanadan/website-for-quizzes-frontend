import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HelloWorldComponent} from "./hello-world/hello-world.component";
import {QuizComponent} from "./quiz/quiz.component";

const routes: Routes = [
  {path: 'hello-world', component: HelloWorldComponent},
  {path: 'quiz', component: QuizComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
