import {Questions} from "./questions-model";

export interface Quiz{
  id?:number;
  subject?: string;
  title?: string;
  questions?: Questions[];

}
