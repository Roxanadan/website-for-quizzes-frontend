import {Options} from "./option-model";

export interface Questions{
  id?:number;
  text?: string;
  options?: Options[];
  correctOptionId:number;

}
