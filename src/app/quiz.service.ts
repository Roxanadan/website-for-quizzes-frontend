import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Quiz} from "./model/quiz-model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private httpClient: HttpClient) { }

  save(quiz: Quiz): Observable<Quiz>{
    return this.httpClient.post<Quiz>("http://localhost:8080/quiz", quiz);
  }
}
