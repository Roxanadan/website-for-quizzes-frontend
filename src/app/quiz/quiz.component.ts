import {Component, OnInit} from '@angular/core';
import {Quiz} from "../model/quiz-model";
import {QuizService} from "../quiz.service";

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  quiz: Quiz = {}

  constructor(private quizService: QuizService) {
  }

  ngOnInit(): void {
  }

  save(): void {
    this.quizService.save(this.quiz).subscribe(response => {
      console.log(response);
    })
  }
}
